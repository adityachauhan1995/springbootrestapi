package com.springBoot.springBootRestApi;

public class RequestLocation {

	 private boolean lat;
	 private boolean longitude;
	
	public boolean isLongitude() {
		return longitude;
	}
	public void setLongitude(boolean longitude) {
		this.longitude = longitude;
	}
	public boolean isLat() {
		return lat;
	}
	public void setLat(boolean lat) {
		this.lat = lat;
	}
}
