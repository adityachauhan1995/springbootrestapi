package com.springBoot.springBootRestApi;

public class RequestBilling {

	 private boolean address;
	 private RequestLocation location;
	public boolean isAddress() {
		return address;
	}
	public void setAddress(boolean address) {
		this.address = address;
	}
	public RequestLocation getLocation() {
		return location;
	}
	public void setLocation(RequestLocation location) {
		this.location = location;
	}
	
}
