package com.springBoot.springBootRestApi;

public class Address {

	 private Billing billing;
	 private Shipping shipping;
	public Billing getBilling() {
		return billing;
	}
	public void setBilling(Billing billing) {
		this.billing = billing;
	}
	public Shipping getShipping() {
		return shipping;
	}
	public void setShipping(Shipping shipping) {
		this.shipping = shipping;
	}
}
