package com.springBoot.springBootRestApi;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@SpringBootApplication
@RestController
public class SpringBootRestApiApplication {

	@Autowired
	CustomerRepo repository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApiApplication.class, args);
	}
	
	@RequestMapping(path = "/selectColumnName", method = RequestMethod.POST)
	public ResponseEntity<?> fetchList(@RequestBody RequestCustomer requestCustomer) throws Exception {
		
		List<Customer> customers = repository.findAll();
		List<Customer> responseCustomers = new ArrayList<>();
		
		if(customers != null) {
			for(Customer customer:customers) {
				Customer tempCustomer = new Customer();
				if(requestCustomer.isFirstName()) {
					tempCustomer.setFirstName(customer.getFirstName());
				}
				if(requestCustomer.isLastName()) {
					tempCustomer.setLastName(customer.getLastName());
				}
				if(requestCustomer.isMobile()) {
					tempCustomer.setMobile(customer.getMobile());
				}
				if(requestCustomer.getAddress() != null) {
					Address address = new Address();
					tempCustomer.setAddress(address);
					if(requestCustomer.getAddress().getBilling() != null) {
						Billing billing = new Billing();
						tempCustomer.getAddress().setBilling(billing);
						if(requestCustomer.getAddress().getBilling().isAddress()) {
							tempCustomer.getAddress().getBilling().setAddress(customer.getAddress().getBilling().getAddress());
						}
						if(requestCustomer.getAddress().getBilling().getLocation() != null) {
							Location location = new Location();
							tempCustomer.getAddress().getBilling().setLocation(location);
							if(requestCustomer.getAddress().getBilling().getLocation().isLat()) {
								tempCustomer.getAddress().getBilling().getLocation().setLat(customer.getAddress().getBilling().getLocation().getLat());
							}
							if(requestCustomer.getAddress().getBilling().getLocation().isLongitude()) {
								tempCustomer.getAddress().getBilling().getLocation().setLongitude(customer.getAddress().getBilling().getLocation().getLongitude());
							}
						}
					}
					if(requestCustomer.getAddress().getShipping() != null) {
						Shipping shipping = new Shipping();
						tempCustomer.getAddress().setShipping(shipping);
						if(requestCustomer.getAddress().getShipping().isAddress()) {
							tempCustomer.getAddress().getShipping().setAddress(customer.getAddress().getShipping().getAddress());
						}
						if(requestCustomer.getAddress().getShipping().getLocation() != null) {
							Location location = new Location();
							tempCustomer.getAddress().getShipping().setLocation(location);
							if(requestCustomer.getAddress().getShipping().getLocation().isLat()) {
								tempCustomer.getAddress().getShipping().getLocation().setLat(customer.getAddress().getShipping().getLocation().getLat());
							}
							if(requestCustomer.getAddress().getShipping().getLocation().isLongitude()) {
								tempCustomer.getAddress().getShipping().getLocation().setLongitude(customer.getAddress().getShipping().getLocation().getLongitude());
							}
						}
					}
				}
				
				responseCustomers.add(tempCustomer);
			}
		}
		
		
		return ResponseEntity.ok(responseCustomers);
	}

}
