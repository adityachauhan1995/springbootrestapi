package com.springBoot.springBootRestApi;

import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "customers")
public class Customer {

	 private String firstName;
	 private String lastName;
	 private String mobile;
	 public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	private Address address;

	
}
