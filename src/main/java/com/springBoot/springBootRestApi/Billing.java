package com.springBoot.springBootRestApi;

public class Billing {
	
	 private String address;
	 private Location location;
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
}
