package com.springBoot.springBootRestApi;


public class RequestCustomer {
	
	 private boolean firstName;
	 private boolean lastName;
	 private boolean mobile;
	 private RequestAddress address;
	public boolean isFirstName() {
		return firstName;
	}
	public void setFirstName(boolean firstName) {
		this.firstName = firstName;
	}
	public boolean isLastName() {
		return lastName;
	}
	public void setLastName(boolean lastName) {
		this.lastName = lastName;
	}
	public boolean isMobile() {
		return mobile;
	}
	public void setMobile(boolean mobile) {
		this.mobile = mobile;
	}
	public RequestAddress getAddress() {
		return address;
	}
	public void setAddress(RequestAddress address) {
		this.address = address;
	}
}
